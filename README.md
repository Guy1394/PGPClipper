# PGPClipper
PGPClipper for Android repository

# Build
Build using Android Studio. Or visit F-Droid for prebuilt release.

# Contribution
You can help translating this app in different languages. Visit https://www.transifex.com/mnetwork/pgpclipper/ for Transifex translation dashboard.

# F-Droid
https://f-droid.org/repository/browse/?fdfilter=pgpclipper&fdid=moe.minori.pgpclipper

# Google Play Store
https://play.google.com/store/apps/details?id=moe.minori.pgpclipper

# Third Party Licenses
OpenPGP API Library, Apache License 2.0

# License
Apache License 2.0, see LICENSE for more details and full license.

# Contributers
Look at contributers tab in repository information. For translation contributers, visit https://www.transifex.com/mnetwork/teams/55577/

# I guess you are lazy.
Yes, I am. Check out release pane or Google Play Store link for more information.
